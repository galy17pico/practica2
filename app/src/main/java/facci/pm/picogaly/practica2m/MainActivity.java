package facci.pm.picogaly.practica2m;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import facci.pm.picogaly.practica2m.R;

public class MainActivity extends AppCompatActivity {

    Button buttonLogin, buttonGuardar, buttonBuscar, buttonPasarParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // tomar el control de los elementos gráficos
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonBuscar= (Button) findViewById(R.id.buttonBuscar);
        buttonGuardar= (Button) findViewById(R.id.buttonGuardar);
        buttonPasarParametro = (Button)findViewById(R.id.buttonPasarParametro);

    //se genera los eventos en los botones
    buttonPasarParametro.setOnClickListener(new View.OnClickListener(){
    @Override
    public void onClick(View view){
    Intent intent = new Intent(MainActivity.this, PasarParametro.class);
    startActivity(intent);
}
});

    buttonLogin.setOnClickListener(new View.OnClickListener(){
    @Override
    public void onClick(View view){
    // crear un objeto de la clase Intent(navegar)
    Intent intent = new Intent(MainActivity.this, MainActivity.class);
    startActivity(intent);
}
});
}
}