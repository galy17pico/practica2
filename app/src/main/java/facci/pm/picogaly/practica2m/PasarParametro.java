package facci.pm.picogaly.practica2m;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import facci.pm.picogaly.practica2m.R;

public class PasarParametro extends AppCompatActivity {

    EditText editTextParametro;
    Button buttonEnviaParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        editTextParametro = (EditText) findViewById(R.id.editTextParametro);
        buttonEnviaParametro = (Button) findViewById(R.id.buttonEnviaParametro);
        buttonEnviaParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PasarParametro.this, RecibirParametro.class);
                //se crea un objeto de tipo que será la vitácora de parámetro a enviar
                Bundle bundle = new Bundle();
                //el método put fija los parámetros a enviar mediante un id
                bundle.putString("dato",editTextParametro.getText().toString());
                //método putExtras envia un objeto de tipo bundle como un solo parámetro entre actividades
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        }
    }

